<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgstocks-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgstocks;

/**
 * ApiComMtgstocksPrintingPriceHistory class file.
 * 
 * This class is a simple implementation of the ApiComMtgstocksPriceHistory.
 * 
 * @author Anastaszor
 */
class ApiComMtgstocksPriceHistory
{
	
	/**
	 * The history of average prices.
	 * 
	 * @var array<integer, ApiComMtgstocksPrice>
	 */
	public array $avg = [];
	
	/**
	 * The history of foil prices.
	 * 
	 * @var array<integer, ApiComMtgstocksPrice>
	 */
	public array $foil = [];
	
	/**
	 * The history of higher prices.
	 * 
	 * @var array<integer, ApiComMtgstocksPrice>
	 */
	public array $high = [];
	
	/**
	 * The history of lower prices.
	 * 
	 * @var array<integer, ApiComMtgstocksPrice>
	 */
	public array $low = [];
	
	/**
	 * The history of market prices.
	 * 
	 * @var array<integer, ApiComMtgstocksPrice>
	 */
	public array $market = [];
	
	/**
	 * The history of market foil prices.
	 * 
	 * @var array<integer, ApiComMtgstocksPrice>
	 */
	public array $marketFoil = [];
	
}
