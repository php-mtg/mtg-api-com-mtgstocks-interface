<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgstocks-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgstocks;

/**
 * MtgstocksArchetypePagination class file.
 * 
 * This is an advanced pagination with additional archetype and format
 * values.
 * 
 * @author Anastaszor
 */
class ApiComMtgstocksArchetypePrez
{
	
	/**
	 * The archetype this prez is on.
	 * 
	 * @var ?ApiComMtgstocksArchetype
	 */
	public ?ApiComMtgstocksArchetype $archetype = null;
	
	/**
	 * The format this prez is on.
	 * 
	 * @var ?ApiComMtgstocksFormat
	 */
	public ?ApiComMtgstocksFormat $format = null;
	
	/**
	 * The decks of this prez.
	 * 
	 * @var array<integer, ApiComMtgstocksDeckAttributes>
	 */
	public array $decks = [];
	
}
