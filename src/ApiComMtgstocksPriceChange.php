<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgstocks-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgstocks;

/**
 * ApiComMtgstocksPriceChange class file.
 * 
 * This represents the different rate of change of prices from different dates
 * in the past.
 * 
 * @author Anastaszor
 */
class ApiComMtgstocksPriceChange
{
	
	/**
	 * The rate of change of the price of the printing on today from yesterday.
	 * 
	 * @var ?float
	 */
	public ?float $yesterday = null;
	
	/**
	 * The rate of change of the price of the printing on today from last week.
	 * 
	 * @var ?float
	 */
	public ?float $lastWeek = null;
	
	/**
	 * The rate of change of the price of the printing on today from last month.
	 * 
	 * @var ?float
	 */
	public ?float $lastMonth = null;
	
}
