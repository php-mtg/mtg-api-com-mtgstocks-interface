<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgstocks-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgstocks;

use Psr\Http\Message\UriInterface;

/**
 * ApiComMtgstocksPrintingPrez class file.
 * 
 * This represents a printing presentation information for a given card
 * in a set.
 * 
 * @author Anastaszor
 */
class ApiComMtgstocksPrintingPrez
{
	
	/**
	 * The id of this printing.
	 * 
	 * @var ?integer
	 */
	public ?int $id = null;
	
	/**
	 * The name of this printing.
	 * 
	 * @var ?string
	 */
	public ?string $name = null;
	
	/**
	 * Whether this printing has content warning.
	 * 
	 * @var ?boolean
	 */
	public ?bool $contentWarning = null;
	
	/**
	 * The slug of this printing.
	 * 
	 * @var ?string
	 */
	public ?string $slug = null;
	
	/**
	 * The rarity of this printing.
	 * 
	 * @var ?string
	 */
	public ?string $rarity = null;
	
	/**
	 * The latest price of this printing.
	 * 
	 * @var ?ApiComMtgstocksLatestPricePrez
	 */
	public ?ApiComMtgstocksLatestPricePrez $latestPrice = null;
	
	/**
	 * The previous price.
	 * 
	 * @var ?float
	 */
	public ?float $previousPrice = null;
	
	/**
	 * The last week price.
	 * 
	 * @var ?float
	 */
	public ?float $lastWeekPrice = null;
	
	/**
	 * The url of the image of the card.
	 * 
	 * @var ?UriInterface
	 */
	public ?UriInterface $image = null;
	
	/**
	 * Whether this printing is foil.
	 * 
	 * @var ?boolean
	 */
	public ?bool $foil = null;
	
}
