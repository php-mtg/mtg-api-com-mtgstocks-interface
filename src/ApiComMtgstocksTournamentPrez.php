<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgstocks-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgstocks;

use DateTimeInterface;

/**
 * ApiComMtgstocksTournamentPrez class file.
 * 
 * This represents the first attributes of the tournament presentation
 * from the archetype page.
 * 
 * @author Anastaszor
 */
class ApiComMtgstocksTournamentPrez
{
	
	/**
	 * The id of the tournament.
	 * 
	 * @var ?integer
	 */
	public ?int $id = null;
	
	/**
	 * The name of the tournament.
	 * 
	 * @var ?string
	 */
	public ?string $name = null;
	
	/**
	 * The number of players in the tournament.
	 * 
	 * @var ?integer
	 */
	public ?int $numPlayers = null;
	
	/**
	 * The type of object (i.e. "tournaments").
	 * 
	 * @var ?string
	 */
	public ?string $tournamenttype = null;
	
	/**
	 * The date of the tournament.
	 * 
	 * @var ?DateTimeInterface
	 */
	public ?DateTimeInterface $date = null;
	
}
