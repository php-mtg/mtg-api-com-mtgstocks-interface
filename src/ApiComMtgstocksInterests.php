<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgstocks-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgstocks;

use DateTimeInterface;

/**
 * ApiComMtgstocksInterests class file.
 * 
 * This represents the interests of the day.
 * 
 * @author Anastaszor
 */
class ApiComMtgstocksInterests
{
	
	/**
	 * The collection of interests for average prices.
	 * 
	 * @var ?ApiComMtgstocksInterestCollection
	 */
	public ?ApiComMtgstocksInterestCollection $average = null;
	
	/**
	 * The date in which this interests are valid.
	 * 
	 * @var ?DateTimeInterface
	 */
	public ?DateTimeInterface $date = null;
	
	/**
	 * The collection of interests for market prices.
	 * 
	 * @var ?ApiComMtgstocksInterestCollection
	 */
	public ?ApiComMtgstocksInterestCollection $market = null;
	
}
