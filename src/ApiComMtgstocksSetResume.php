<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgstocks-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgstocks;

/**
 * ApiComMtgstocksSetResume class file.
 * 
 * This represents a set presentation information.
 * 
 * @author Anastaszor
 */
class ApiComMtgstocksSetResume
{
	
	/**
	 * The identifier of the set.
	 * 
	 * @var ?integer
	 */
	public ?int $id = null;
	
	/**
	 * The name of the set.
	 * 
	 * @var ?string
	 */
	public ?string $name = null;
	
	/**
	 * The code of the set.
	 * 
	 * @var ?string
	 */
	public ?string $abbreviation = null;
	
	/**
	 * The class of the icon to render (set symbol).
	 * 
	 * @var ?string
	 */
	public ?string $iconClass = null;
	
	/**
	 * The type of set.
	 * 
	 * @var ?string
	 */
	public ?string $setType = null;
	
	/**
	 * The slug of the set name.
	 * 
	 * @var ?string
	 */
	public ?string $slug = null;
	
	/**
	 * The id of the mkm id for this set.
	 * 
	 * @var ?integer
	 */
	public ?int $mkmId = null;
	
}
