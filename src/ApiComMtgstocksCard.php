<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgstocks-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgstocks;

use Psr\Http\Message\UriInterface;

/**
 * ApiComMtgstocksCard class file.
 * 
 * This represents a specific card not from any extension.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.TooManyFields")
 */
class ApiComMtgstocksCard
{
	
	/**
	 * The id of the card.
	 * 
	 * @var ?integer
	 */
	public ?int $id = null;
	
	/**
	 * The manacost of the card.
	 * 
	 * @var ?string
	 */
	public ?string $manaCost = null;
	
	/**
	 * The name of the card.
	 * 
	 * @var ?string
	 */
	public ?string $name = null;
	
	/**
	 * Whether this card has content warning.
	 * 
	 * @var ?boolean
	 */
	public ?bool $contentWarning = null;
	
	/**
	 * The oracle text of the card (html).
	 * 
	 * @var ?string
	 */
	public ?string $oracle = null;
	
	/**
	 * The full mana cost of the card.
	 * 
	 * @var ?string
	 */
	public ?string $cost = null;
	
	/**
	 * The fist half of the cost.
	 * 
	 * @var array<integer, string>
	 */
	public array $splitcost = [];
	
	/**
	 * The second half of the cost.
	 * 
	 * @var array<integer, string>
	 */
	public array $splitcost2 = [];
	
	/**
	 * The converted mana cost of the card.
	 * 
	 * @var ?integer
	 */
	public ?int $cmc = null;
	
	/**
	 * The color of the card.
	 * 
	 * @var ?string
	 */
	public ?string $color = null;
	
	/**
	 * The url of the edhrec page for this card.
	 * 
	 * @var ?UriInterface
	 */
	public ?UriInterface $edhrecUrl = null;
	
	/**
	 * The power and toughness value.
	 * 
	 * @var ?string
	 */
	public ?string $pwrtgh = null;
	
	/**
	 * The power.
	 * 
	 * @var ?string
	 */
	public ?string $power = null;
	
	/**
	 * The toughness.
	 * 
	 * @var ?string
	 */
	public ?string $toughness = null;
	
	/**
	 * The loyalty.
	 * 
	 * @var ?string
	 */
	public ?string $loyalty = null;
	
	/**
	 * The supertype and type part of the typeline.
	 * 
	 * @var ?string
	 */
	public ?string $supertype = null;
	
	/**
	 * The subtype part of the typeline.
	 * 
	 * @var ?string
	 */
	public ?string $subtype = null;
	
	/**
	 * Whether this card is on the reserved list.
	 * 
	 * @var ?boolean
	 */
	public ?bool $reserved = null;
	
	/**
	 * The lowest printing id for this card.
	 * 
	 * @var ?integer
	 */
	public ?int $lowestPrintId = null;
	
	/**
	 * The lowest printing for this card.
	 * 
	 * @var ?integer
	 */
	public ?int $lowestPrint = null;
	
	/**
	 * The legality of the card.
	 * 
	 * @var ?ApiComMtgstocksCardLegality
	 */
	public ?ApiComMtgstocksCardLegality $legal = null;
	
}
