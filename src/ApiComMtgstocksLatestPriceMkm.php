<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgstocks-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgstocks;

/**
 * ApiComMtgstocksLatestPriceMkm class file.
 * 
 * This represents the latest price seen on magiccardmarket.eu website.
 * 
 * @author Anastaszor
 */
class ApiComMtgstocksLatestPriceMkm
{
	
	/**
	 * The average value of the price.
	 * 
	 * @var ?float
	 */
	public ?float $avg = null;
	
	/**
	 * The lower value of the price.
	 * 
	 * @var ?float
	 */
	public ?float $low = null;
	
}
