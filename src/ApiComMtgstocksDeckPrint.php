<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgstocks-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgstocks;

use Psr\Http\Message\UriInterface;

/**
 * ApiComMtgstocksDeckPrint class file.
 * 
 * This interface represents a printing of a given deck.
 * 
 * @author Anastaszor
 */
class ApiComMtgstocksDeckPrint
{
	
	/**
	 * Gets the identifier of the printing.
	 *
	 * @var ?integer
	 */
	public ?int $id = null;
	
	/**
	 * Gets the arena id number of the printing.
	 * 
	 * @var ?integer
	 */
	public ?int $arenaId = null;
	
	/**
	 * Gets the collector number of the printing.
	 * 
	 * @var ?integer
	 */
	public ?int $collectorNumber = null;
	
	/**
	 * Gets whether this print is foil.
	 * 
	 * @var ?boolean
	 */
	public ?bool $foil = null;
	
	/**
	 * The slug of the name.
	 * 
	 * @var ?string
	 */
	public ?string $slug = null;
	
	/**
	 * Gets the uri of the image of the printing.
	 * 
	 * @var ?UriInterface
	 */
	public ?UriInterface $image = null;
	
	/**
	 * Gets the price statistics.
	 * 
	 * @var ?ApiComMtgstocksLatestPriceStat
	 */
	public ?ApiComMtgstocksLatestPriceStat $latestPrice = null;
	
	/**
	 * Gets the price from cardkingdom.
	 * 
	 * @var ?float
	 */
	public ?float $latestPriceCk = null;
	
	/**
	 * Gets the set this printing is in.
	 * 
	 * @var ?ApiComMtgstocksSetResume
	 */
	public ?ApiComMtgstocksSetResume $set = null;
	
}
