<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgstocks-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgstocks;

/**
 * ApiComMtgstocksPrintingOther class file.
 * 
 * This represents a resumee of the other printings this card is involved
 * in (i.e. past and future reeditions).
 * 
 * @author Anastaszor
 */
class ApiComMtgstocksPrintingOther
{
	
	/**
	 * The id of the other printing.
	 * 
	 * @var ?integer
	 */
	public ?int $id = null;
	
	/**
	 * The name of this card on this printing.
	 * 
	 * @var ?string
	 */
	public ?string $name = null;
	
	/**
	 * The rarity of this printing.
	 * 
	 * @var ?string
	 */
	public ?string $rarity = null;
	
	/**
	 * The id of the set for this id.
	 * 
	 * @var ?integer
	 */
	public ?int $setId = null;
	
	/**
	 * The name of the set for this printing.
	 * 
	 * @var ?string
	 */
	public ?string $setName = null;
	
	/**
	 * The icon class of the printing.
	 * 
	 * @var ?string
	 */
	public ?string $iconClass = null;
	
	/**
	 * The latest price value.
	 * 
	 * @var ?float
	 */
	public ?float $latestPrice = null;
	
	/**
	 * The latest price value from mkm.
	 * 
	 * @var ?float
	 */
	public ?float $latestPriceMkm = null;
	
	/**
	 * Whether this printing is legal.
	 * 
	 * @var ?boolean
	 */
	public ?bool $legal = null;
	
}
