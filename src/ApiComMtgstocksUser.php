<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgstocks-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgstocks;

/**
 * ApiComMtgstocksUser class file.
 * 
 * This represents a mtgstocks user.
 * 
 * @author Anastaszor
 */
class ApiComMtgstocksUser
{
	
	/**
	 * The user id.
	 * 
	 * @var ?string
	 */
	public ?string $id = null;
	
}
