<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgstocks-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgstocks;

/**
 * ApiComMtgstocksCardLegality class file.
 * 
 * This represents the legalities of a card across all the known formats.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.TooManyFields")
 */
class ApiComMtgstocksCardLegality
{
	
	/**
	 * The legality for the alchemy format.
	 *
	 * @var ?string
	 */
	public ?string $alchemy = null;
	
	/**
	 * The legality for the brawl format.
	 *
	 * @var ?string
	 */
	public ?string $brawl = null;
	
	/**
	 * The legality for the commander format.
	 *
	 * @var ?string
	 */
	public ?string $commander = null;
	
	/**
	 * The legality for the duel commander format.
	 *
	 * @var ?string
	 */
	public ?string $duel = null;
	
	/**
	 * The legality for the explorer format.
	 *
	 * @var ?string
	 */
	public ?string $explorer = null;
	
	/**
	 * The legality for the frontier format.
	 *
	 * @var ?string
	 */
	public ?string $frontier = null;
	
	/**
	 * The legality for the future format.
	 *
	 * @var ?string
	 */
	public ?string $future = null;
	
	/**
	 * The legality for the gladiator format.
	 *
	 * @var ?string
	 */
	public ?string $gladiator = null;
	
	/**
	 * The legality for the historic format.
	 *
	 * @var ?string
	 */
	public ?string $historic = null;
	
	/**
	 * The legality for the historic brawl format.
	 *
	 * @var ?string
	 */
	public ?string $historicbrawl = null;
	
	/**
	 * The legality for the legacy format.
	 * 
	 * @var ?string
	 */
	public ?string $legacy = null;
	
	/**
	 * The legality for the modern format.
	 * 
	 * @var ?string
	 */
	public ?string $modern = null;
	
	/**
	 * The legality for the oathbreaker format.
	 * 
	 * @var ?string
	 */
	public ?string $oathbreaker = null;
	
	/**
	 * The legality for the old school format.
	 *
	 * @var ?string
	 */
	public ?string $oldschool = null;
	
	/**
	 * The legality for the pauper format.
	 *
	 * @var ?string
	 */
	public ?string $pauper = null;
	
	/**
	 * The legality for the pauper commander format.
	 *
	 * @var ?string
	 */
	public ?string $paupercommander = null;
	
	/**
	 * The legality for the penny format.
	 *
	 * @var ?string
	 */
	public ?string $penny = null;
	
	/**
	 * The legality for the predh format.
	 * 
	 * @var ?string
	 */
	public ?string $predh = null;
	
	/**
	 * The legality for the premodern format.
	 *
	 * @var ?string
	 */
	public ?string $premodern = null;
	
	/**
	 * The legality for the pioneer format.
	 *
	 * @var ?string
	 */
	public ?string $pioneer = null;
	
	/**
	 * The legality for the standard format.
	 *
	 * @var ?string
	 */
	public ?string $standard = null;
	
	/**
	 * The legality for the vintage format.
	 * 
	 * @var ?string
	 */
	public ?string $vintage = null;
	
}
