<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgstocks-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgstocks;

use DateTimeInterface;

/**
 * ApiComMtgstocksTournament class file.
 * 
 * This represents a tournament.
 * 
 * @author Anastaszor
 */
class ApiComMtgstocksTournament
{
	
	/**
	 * The id of the tournament.
	 * 
	 * @var ?integer
	 */
	public ?int $id = null;
	
	/**
	 * The name of the tournament.
	 * 
	 * @var ?string
	 */
	public ?string $name = null;
	
	/**
	 * The date of the tournament.
	 * 
	 * @var ?DateTimeInterface
	 */
	public ?DateTimeInterface $date = null;
	
	/**
	 * The number of players of this tournament.
	 * 
	 * @var ?integer
	 */
	public ?int $numPlayers = null;
	
	/**
	 * The type of tournament of this tournament.
	 * 
	 * @var ?string
	 */
	public ?string $tournamenttype = null;
	
	/**
	 * The format in which this tournament was played.
	 * 
	 * @var ?string
	 */
	public ?string $format = null;
	
	/**
	 * The decks that were played in this tournament and their positions.
	 * 
	 * @var array<integer, ApiComMtgstocksDeckPlacement>
	 */
	public array $decks = [];
	
}
