<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgstocks-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgstocks;

use DateTimeInterface;

/**
 * ApiComMtgstocksDeck class file.
 * 
 * This represents a complete deck that performed in a tournament.
 * 
 * @author Anastaszor
 */
class ApiComMtgstocksDeck
{
	
	/**
	 * The id of the deck.
	 *
	 * @var ?integer
	 */
	public ?int $id = null;
	
	/**
	 * The archetype this deck falls in.
	 * 
	 * @var ?ApiComMtgstocksArchetype
	 */
	public ?ApiComMtgstocksArchetype $archetype = null;
	
	/**
	 * The card that is used as commander, if any.
	 * 
	 * @var ?ApiComMtgstocksCard
	 */
	public ?ApiComMtgstocksCard $commander = null;
	
	/**
	 * The cards that are used as commanders, if multiple.
	 * 
	 * @var array<integer, ApiComMtgstocksCard>
	 */
	public array $commanders = [];
	
	/**
	 * The card that is used as partner commander, if any.
	 * 
	 * @var ?ApiComMtgstocksCard
	 */
	public ?ApiComMtgstocksCard $commanderPartner = null;
	
	/**
	 * The date when this deck was played.
	 * 
	 * @var ?DateTimeInterface
	 */
	public ?DateTimeInterface $date = null;
	
	/**
	 * Whether this deck is editable.
	 * 
	 * @var ?boolean
	 */
	public ?bool $editable = null;
	
	/**
	 * The format in which the deck falls.
	 * 
	 * @var ?ApiComMtgstocksFormat
	 */
	public ?ApiComMtgstocksFormat $format = null;
	
	/**
	 * The name of the deck.
	 * 
	 * @var ?string
	 */
	public ?string $name = null;
	
	/**
	 * The name of the player that piloted the deck.
	 * 
	 * @var ?string
	 */
	public ?string $player = null;
	
	/**
	 * The position of the deck at the end of the tournament it was played.
	 * 
	 * @var ?integer
	 */
	public ?int $position = null;
	
	/**
	 * The id of the tournament it was played.
	 * 
	 * @var ?integer
	 */
	public ?int $tournamentId = null;
	
	/**
	 * The cards that composed the main board of this deck.
	 * 
	 * @var array<integer, ApiComMtgstocksCardQuantity>
	 */
	public array $mainboard = [];
	
	/**
	 * The cards that composed the side board of this deck.
	 * 
	 * @var array<integer, ApiComMtgstocksCardQuantity>
	 */
	public array $sideboard = [];
	
	/**
	 * The user of that created this deck.
	 * 
	 * @var ?ApiComMtgstocksUser
	 */
	public ?ApiComMtgstocksUser $user = null;
	
}
