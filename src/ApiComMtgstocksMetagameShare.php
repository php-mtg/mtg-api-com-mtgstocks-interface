<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgstocks-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgstocks;

use DateTimeInterface;

/**
 * ApiComMtgstocksMetagameShare class file.
 * 
 * This represents a current overview of the metagame for a specific
 * format and a specific archetype.
 * 
 * @author Anastaszor
 */
class ApiComMtgstocksMetagameShare
{
	
	/**
	 * The archetype data about the metagame position.
	 * 
	 * @var ?ApiComMtgstocksArchetype
	 */
	public ?ApiComMtgstocksArchetype $archetype = null;
	
	/**
	 * The date when this stat is valid.
	 * 
	 * @var ?DateTimeInterface
	 */
	public ?DateTimeInterface $date = null;
	
	/**
	 * The format in which this archetype is played.
	 * 
	 * @var ?ApiComMtgstocksFormat
	 */
	public ?ApiComMtgstocksFormat $format = null;
	
	/**
	 * The date of the next event for this format.
	 * 
	 * @var ?DateTimeInterface
	 */
	public ?DateTimeInterface $nextdate = null;
	
	/**
	 * The number of top8 that archetype made.
	 * 
	 * @var ?integer
	 */
	public ?int $placings = null;
	
	/**
	 * The date of the previous event for this format.
	 * 
	 * @var ?DateTimeInterface
	 */
	public ?DateTimeInterface $previousDate = null;
	
	/**
	 * The total quantity of places that were available.
	 * 
	 * @var ?integer
	 */
	public ?int $total = null;
	
}
