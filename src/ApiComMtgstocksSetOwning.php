<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgstocks-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgstocks;

use Psr\Http\Message\UriInterface;

/**
 * ApiComMtgstocksSetOwning class file.
 *
 * This represents a set information with all the cards printed in the
 * set.
 *
 * @author Anastaszor
 */
class ApiComMtgstocksSetOwning
{
	
	/**
	 * The identifier of the set.
	 *
	 * @var ?integer
	 */
	public ?int $id = null;
	
	/**
	 * The name of the set.
	 *
	 * @var ?string
	 */
	public ?string $name = null;
	
	/**
	 * Whether this set has content warnings.
	 * 
	 * @var ?boolean
	 */
	public ?bool $contentWarning = null;
	
	/**
	 * The slug of the set.
	 * 
	 * @var ?string
	 */
	public ?string $slug = null;
	
	/**
	 * The class of the icon to render (set symbol).
	 *
	 * @var ?string
	 */
	public ?string $iconClass = null;
	
	/**
	 * The uri of the image of this set.
	 * 
	 * @var ?UriInterface
	 */
	public ?UriInterface $image = null;
	
	/**
	 * Whether the card of this set is legal.
	 * 
	 * @var ?boolean
	 */
	public ?bool $legal = null;
	
	/**
	 * The rarity of the card in this set.
	 * 
	 * @var ?string
	 */
	public ?string $rarity = null;
	
	/**
	 * The id of the set.
	 * 
	 * @var ?integer
	 */
	public ?int $setId = null;
	
	/**
	 * The name of the set.
	 *
	 * @var ?string
	 */
	public ?string $setName = null;
	
	/**
	 * The latest price.
	 * 
	 * @var ?float
	 */
	public ?float $latestPrice = null;
	
	/**
	 * The latest price price mkm.
	 * 
	 * @var ?float
	 */
	public ?float $latestPriceMkm = null;
	
}
