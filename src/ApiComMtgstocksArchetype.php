<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgstocks-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgstocks;

use Psr\Http\Message\UriInterface;

/**
 * ApiComMtgstocksArchetype class file.
 * 
 * This represents an archetype.
 * 
 * @author Anastaszor
 */
class ApiComMtgstocksArchetype
{
	
	/**
	 * The id of the archetype.
	 * 
	 * @var ?integer
	 */
	public ?int $id = null;
	
	/**
	 * The colors of this archetype.
	 *
	 * @var array<integer, string>
	 */
	public array $colors = [];
	
	/**
	 * The name of the archetype.
	 * 
	 * @var ?string
	 */
	public ?string $name = null;
	
	/**
	 * The uri of the image for this archetype.
	 *
	 * @var ?UriInterface
	 */
	public ?UriInterface $image = null;
	
	/**
	 * Whether this archetype is obsolete.
	 * 
	 * @var ?boolean
	 */
	public ?bool $old = null;
	
	/**
	 * The id of the format for this archetype.
	 * 
	 * @var ?integer
	 */
	public ?int $formatId = null;
	
}
