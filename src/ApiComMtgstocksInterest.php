<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgstocks-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgstocks;

use DateTimeInterface;

/**
 * ApiComMtgstocksInterest class file.
 * 
 * This represents the interests for card price moves.
 * 
 * @author Anastaszor
 */
class ApiComMtgstocksInterest
{
	
	/**
	 * When this interest was valid.
	 * 
	 * @var ?DateTimeInterface
	 */
	public ?DateTimeInterface $date = null;
	
	/**
	 * Whether this interest is on a foil price.
	 * 
	 * @var ?boolean
	 */
	public ?bool $foil = null;
	
	/**
	 * The type of interest (meaning the period of time between the past and
	 * the present price).
	 * 
	 * @var ?string
	 */
	public ?string $interestType = null;
	
	/**
	 * The past price value.
	 * 
	 * @var ?float
	 */
	public ?float $pastPrice = null;
	
	/**
	 * The percentage of move from the past price.
	 * 
	 * @var ?float
	 */
	public ?float $percentage = null;
	
	/**
	 * The present price value.
	 * 
	 * @var ?float
	 */
	public ?float $presentPrice = null;
	
	/**
	 * The related card.
	 * 
	 * @var ?ApiComMtgstocksPrinting
	 */
	public ?ApiComMtgstocksPrinting $print = null;
	
}
