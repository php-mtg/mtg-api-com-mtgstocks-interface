<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgstocks-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgstocks;

/**
 * ApiComMtgstocksLatestPriceStat class file.
 * 
 * This interface represents the stats prices correspond to the card of a given
 * deck.
 * 
 * @author Anastaszor
 */
class ApiComMtgstocksLatestPriceStat
{
	
	/**
	 * Gets the average value.
	 * 
	 * @var ?float
	 */
	public ?float $avg = null;
	
	/**
	 * Gets the high value.
	 * 
	 * @var ?float
	 */
	public ?float $high = null;
	
	/**
	 * Gets the low value.
	 * 
	 * @var ?float
	 */
	public ?float $low = null;
	
}
