<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgstocks-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgstocks;

/**
 * ApiComMtgstocksDeckCard class file.
 * 
 * This interface represents a card that is present in a deck.
 * 
 * @author Anastaszor
 */
class ApiComMtgstocksDeckCard
{
	
	/**
	 * Gets the type of the deck card.
	 * 
	 * @var ?string
	 */
	public ?string $cardType = null;
	
	/**
	 * Gets the converted mana cost of the deck card.
	 * 
	 * @var ?integer
	 */
	public ?int $cmc = null;
	
	/**
	 * Gets the color of the deck card.
	 * 
	 * @var ?string
	 */
	public ?string $color = null;
	
	/**
	 * Gets the colors of the card.
	 * 
	 * @var array<integer, string>
	 */
	public array $colors = [];
	
	/**
	 * Gets the color identity of the card.
	 * 
	 * @var array<integer, string>
	 */
	public array $colorIdentity = [];
	
	/**
	 * Gets whether this card has content warning.
	 * 
	 * @var ?boolean
	 */
	public ?bool $contentWarning = null;
	
	/**
	 * Gets the cost of this card.
	 * 
	 * @var ?string
	 */
	public ?string $cost = null;
	
	/**
	 * Gets the mana cost of this card.
	 * 
	 * @var ?string
	 */
	public ?string $manaCost = null;
	
	/**
	 * Gets the name of the deck card.
	 * 
	 * @var ?string
	 */
	public ?string $name = null;
	
	/**
	 * Gets the printing with the lowest id.
	 * 
	 * @var ?ApiComMtgstocksDeckPrint
	 */
	public ?ApiComMtgstocksDeckPrint $lowestPrint = null;
	
}
