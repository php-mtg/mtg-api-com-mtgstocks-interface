<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgstocks-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgstocks;

use PhpExtended\DataProvider\UnprovidableThrowable;
use PhpExtended\Reifier\ReificationThrowable;
use Psr\Http\Client\ClientExceptionInterface;
use Stringable;

/**
 * ApiComMtgstocksEndpoint class file.
 * 
 * This is the entry point for all methods using this api.
 * 
 * @author Anastaszor
 */
interface ApiComMtgstocksEndpointInterface extends Stringable
{
	
	/**
	 * Gets the information with all the sets.
	 * 
	 * @return array<integer, ApiComMtgstocksSetPrez>
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getSetsInformation() : array;
	
	/**
	 * Gets the information for given set with all the printing informations.
	 * 
	 * @param integer $setId the id of the set
	 * @return ApiComMtgstocksSet
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getSetPrintings(int $setId) : ApiComMtgstocksSet;
	
	/**
	 * Gets the information for given printing with its price information for
	 * today.
	 * 
	 * @param integer $printingId the id of the printing
	 * @return ApiComMtgstocksPrinting
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getPrinting(int $printingId) : ApiComMtgstocksPrinting;
	
	/**
	 * Gets the prices informations for given printing with all the price
	 * history.
	 * 
	 * @param integer $printingId the id of the printing
	 * @return ApiComMtgstocksPriceHistory
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getPrintingPrices(int $printingId) : ApiComMtgstocksPriceHistory;
	
	/**
	 * Gets the links information for given printing.
	 * 
	 * @param integer $printingId the id of the printing
	 * @return array<integer, ApiComMtgstocksLink>
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getPrintingLinks(int $printingId) : array;
	
	/**
	 * Gets the archetype stats from the given printing.
	 * 
	 * @param integer $cardId the id of the printing
	 * @return array<integer, ApiComMtgstocksArchetypeStat>
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getCardArchetypePresences(int $cardId) : array;
	
	/**
	 * Gets the deck information.
	 * 
	 * @param integer $deckId
	 * @return ApiComMtgstocksDeck
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getDeck(int $deckId) : ApiComMtgstocksDeck;
	
	/**
	 * Gets the deck stats from the given printing for the given api, ordered
	 * by date decreasing.
	 * 
	 * @param integer $archetypeId
	 * @param integer $printingId
	 * @param integer $pagenb
	 * @return array<ApiComMtgstocksDeckAttributes>
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getDecksByArchetypeAndPrinting(int $archetypeId, int $printingId, int $pagenb = 1) : array;
	
	/**
	 * Gets the deck from the given archetype, ordered by date decreasing.
	 * 
	 * @param integer $archetypeId
	 * @param integer $pagenb
	 * @return ApiComMtgstocksArchetypePrez
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getDecksByArchetype(int $archetypeId, int $pagenb = 1) : ApiComMtgstocksArchetypePrez;
	
	/**
	 * Gets the informations for given tournament.
	 * 
	 * @param integer $tournamentId
	 * @return ApiComMtgstocksTournament
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getTournament(int $tournamentId) : ApiComMtgstocksTournament;
	
	/**
	 * Gets the tournaments from the given format, ordered by date decreasing.
	 * 
	 * @param integer $formatId
	 * @param integer $pagenb
	 * @return array<ApiComMtgstocksTournamentPrez>
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getTournamentsByFormat(int $formatId, int $pagenb = 1) : array;
	
	/**
	 * Gets the archetypes from the given format.
	 * 
	 * @param integer $formatId
	 * @return array<integer, ApiComMtgstocksArchetype>
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getArchetypesByFormat(int $formatId) : array;
	
	/**
	 * Gets the archetype results for the given format.
	 * 
	 * @param integer $formatId
	 * @return array<integer, ApiComMtgstocksMetagameShare>
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getMetagame(int $formatId) : array;
	
	/**
	 * Gets the format list.
	 * 
	 * @return array<integer, ApiComMtgstocksFormat>
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getFormats() : array;
	
	/**
	 * Gets the interests of the day.
	 * 
	 * @return ApiComMtgstocksInterests
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getInterests() : ApiComMtgstocksInterests;
	
}
