<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgstocks-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgstocks;

use DateTimeInterface;
use Psr\Http\Message\UriInterface;

/**
 * ApiComMtgstocksLink class file.
 * 
 * This represents the informations about a single article.
 * 
 * @author Anastaszor
 */
class ApiComMtgstocksLink
{
	
	/**
	 * The name of the author.
	 * 
	 * @var ?string
	 */
	public ?string $author = null;
	
	/**
	 * The date of publication of the article.
	 * 
	 * @var ?DateTimeInterface
	 */
	public ?DateTimeInterface $date = null;
	
	/**
	 * The name of the source of the article.
	 * 
	 * @var ?string
	 */
	public ?string $source = null;
	
	/**
	 * Gets the title of the article.
	 * 
	 * @var ?string
	 */
	public ?string $title = null;
	
	/**
	 * Gets the url of the article.
	 * 
	 * @var ?UriInterface
	 */
	public ?UriInterface $url = null;
	
}
