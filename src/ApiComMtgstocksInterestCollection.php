<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgstocks-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgstocks;

/**
 * ApiComMtgstocksInterestCollection class file.
 * 
 * This is an aggregator for interests.
 * 
 * @author Anastaszor
 */
class ApiComMtgstocksInterestCollection
{
	
	/**
	 * The interests over the foil prices.
	 * 
	 * @var array<integer, ApiComMtgstocksInterest>
	 */
	public array $foil = [];
	
	/**
	 * The interests over the normal prices.
	 * 
	 * @var array<integer, ApiComMtgstocksInterest>
	 */
	public array $normal = [];
	
}
