<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgstocks-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgstocks;

use DateTimeInterface;

/**
 * ApiComMtgstocksSetPrez class file.
 * 
 * This represents a set presentation information with all the cards
 * printed in the set.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.TooManyFields")
 * @SuppressWarnings("PHPMD.ShortVariableName")
 */
class ApiComMtgstocksSetPrez
{
	
	/**
	 * The identifier of the set.
	 * 
	 * @var ?integer
	 */
	public ?int $id = null;
	
	/**
	 * The name of the set.
	 * 
	 * @var ?string
	 */
	public ?string $name = null;
	
	/**
	 * The slug of the set.
	 * 
	 * @var ?string
	 */
	public ?string $slug = null;
	
	/**
	 * The code of the set.
	 * 
	 * @var ?string
	 */
	public ?string $abbreviation = null;
	
	/**
	 * When this set was released.
	 * 
	 * @var ?DateTimeInterface
	 */
	public ?DateTimeInterface $date = null;
	
	/**
	 * The class of the icon to render (set symbol).
	 * 
	 * @var ?string
	 */
	public ?string $iconClass = null;
	
	/**
	 * The type of set.
	 * 
	 * @var ?string
	 */
	public ?string $setType = null;
	
	/**
	 * @var ?boolean
	 */
	public ?bool $ev = null;
	
	/**
	 * Whether this set has expected value.
	 *
	 * @var ?boolean
	 */
	public ?bool $hasEv = null;
	
	/**
	 * Whether this set has sealed products.
	 *
	 * @var ?boolean
	 */
	public ?bool $hasSealed = null;
	
}
