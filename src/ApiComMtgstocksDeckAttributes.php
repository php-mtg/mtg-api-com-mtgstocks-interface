<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgstocks-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgstocks;

use DateTimeInterface;

/**
 * ApiComMtgstocksDeckAttributes class file.
 * 
 * This represents information about the deck in the tournament it
 * was played.
 * 
 * @author Anastaszor
 */
class ApiComMtgstocksDeckAttributes
{
	
	/**
	 * The id of the deck.
	 *
	 * @var ?integer
	 */
	public ?int $id = null;
	
	/**
	 * Gets the archetype name from which this deck is made of.
	 * 
	 * @var ?string
	 */
	public ?string $archetype = null;
	
	/**
	 * The date this deck was played.
	 * 
	 * @var ?DateTimeInterface
	 */
	public ?DateTimeInterface $date = null;
	
	/**
	 * The number of players in the tournament this deck was played.
	 * 
	 * @var ?integer
	 */
	public ?int $numPlayers = null;
	
	/**
	 * The name of the player that piloted this deck in the tournament.
	 * 
	 * @var ?string
	 */
	public ?string $player = null;
	
	/**
	 * The position the player placed this deck in the tournament.
	 * 
	 * @var ?integer
	 */
	public ?int $position = null;
	
	/**
	 * The tournament this deck was played on.
	 * 
	 * @var ?ApiComMtgstocksTournament
	 */
	public ?ApiComMtgstocksTournament $tournament = null;
	
}
