<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgstocks-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgstocks;

/**
 * ApiComMtgstocksDeckPlacement class file.
 * 
 * This represents a tournament result for a given deck at a given 
 * tournament.
 * 
 * @author Anastaszor
 */
class ApiComMtgstocksDeckPlacement
{
	
	/**
	 * The id of the deck.
	 *
	 * @var ?integer
	 */
	public ?int $id = null;
	
	/**
	 * The archetype of target deck.
	 * 
	 * @var ?ApiComMtgstocksArchetype
	 */
	public ?ApiComMtgstocksArchetype $archetype = null;
	
	/**
	 * The player that piloted the deck.
	 * 
	 * @var ?string
	 */
	public ?string $player = null;
	
	/**
	 * The position of the deck at the end of the tournament.
	 * 
	 * @var ?integer
	 */
	public ?int $position = null;
	
}
