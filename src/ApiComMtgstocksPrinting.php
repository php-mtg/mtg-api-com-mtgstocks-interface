<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgstocks-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgstocks;

use PhpExtended\Uuid\UuidInterface;
use Psr\Http\Message\UriInterface;

/**
 * ApiComMtgstocksPrinting class file.
 * 
 * This represents a printing information for a given card in a set.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.TooManyFields")
 */
class ApiComMtgstocksPrinting
{
	
	/**
	 * The id of this printing.
	 * 
	 * @var ?integer
	 */
	public ?int $id = null;
	
	/**
	 * The name of this printing.
	 * 
	 * @var ?string
	 */
	public ?string $name = null;
	
	/**
	 * The slug of this printing.
	 * 
	 * @var ?string
	 */
	public ?string $slug = null;
	
	/**
	 * The arena id for this printing.
	 * 
	 * @var ?integer
	 */
	public ?int $arenaId = null;
	
	/**
	 * The arena abbrev for this printing.
	 * 
	 * @var ?string
	 */
	public ?string $arenaAbbreviation = null;
	
	/**
	 * The buy list prices.
	 * 
	 * @var ?ApiComMtgstocksBuylist
	 */
	public ?ApiComMtgstocksBuylist $buyList = null;
	
	/**
	 * The collector number for this printing.
	 * 
	 * @var ?integer
	 */
	public ?int $collectorNumber = null;
	
	/**
	 * The cardprinting id of this printing.
	 * 
	 * @var ?integer
	 */
	public ?int $cardtraderId = null;
	
	/**
	 * The rate of change of the prices.
	 * 
	 * @var ?ApiComMtgstocksPriceChange
	 */
	public ?ApiComMtgstocksPriceChange $change = null;
	
	/**
	 * The tcgplayer id for this printing.
	 * 
	 * @var ?integer
	 */
	public ?int $tcgId = null;
	
	/**
	 * The tcgplayer url for this printing.
	 * 
	 * @var ?UriInterface
	 */
	public ?UriInterface $tcgUrl = null;
	
	/**
	 * The magiccardmarket id for this printing.
	 * 
	 * @var ?integer
	 */
	public ?int $mkmId = null;
	
	/**
	 * The magiccardmarket url for this printing.
	 * 
	 * @var ?UriInterface
	 */
	public ?UriInterface $mkmUrl = null;
	
	/**
	 * The scryfall id for this printing.
	 * 
	 * @var ?UuidInterface
	 */
	public ?UuidInterface $scryfallId = null;
	
	/**
	 * The rarity of this printing.
	 * 
	 * @var ?string
	 */
	public ?string $rarity = null;
	
	/**
	 * The extended rarity of this printing.
	 * 
	 * @var ?string
	 */
	public ?string $extendedRarity = null;
	
	/**
	 * The all time high price for this printing.
	 * 
	 * @var ?ApiComMtgstocksAllTimeHigh
	 */
	public ?ApiComMtgstocksAllTimeHigh $allTimeHigh = null;
	
	/**
	 * The all time low price for this printing.
	 * 
	 * @var ?ApiComMtgstocksAllTimeLow
	 */
	public ?ApiComMtgstocksAllTimeLow $allTimeLow = null;
	
	/**
	 * The multiverseid for this printing.
	 * 
	 * @var ?integer
	 */
	public ?int $multiverseId = null;
	
	/**
	 * The latest price of this printing.
	 * 
	 * @var ?ApiComMtgstocksLatestPrice
	 */
	public ?ApiComMtgstocksLatestPrice $latestPrice = null;
	
	/**
	 * The latest known price on magiccardmarket.
	 * 
	 * @var ?ApiComMtgstocksLatestPriceMkm
	 */
	public ?ApiComMtgstocksLatestPriceMkm $latestPriceMkm = null;
	
	/**
	 * The latest known price on cardkingdom.
	 * 
	 * @var ?ApiComMtgstocksLatestPriceCk
	 */
	public ?ApiComMtgstocksLatestPriceCk $latestPriceCk = null;
	
	/**
	 * The latest known price on cardtrader.
	 * 
	 * @var ?ApiComMtgstocksLatestPriceMkm
	 */
	public ?ApiComMtgstocksLatestPriceMkm $latestPriceCardtrader = null;
	
	/**
	 * The latest price on miniaturemarket.
	 * 
	 * @var ?ApiComMtgstocksLatestPriceCk
	 */
	public ?ApiComMtgstocksLatestPriceCk $latestPriceMm = null;
	
	/**
	 * The previous price.
	 * 
	 * @var ?float
	 */
	public ?float $previousPrice = null;
	
	/**
	 * The last week price.
	 * 
	 * @var ?float
	 */
	public ?float $lastWeekPrice = null;
	
	/**
	 * Whether to include this printing by default.
	 * 
	 * @var ?boolean
	 */
	public ?bool $includeDefault = null;
	
	/**
	 * The url of the image of the card.
	 * 
	 * @var ?UriInterface
	 */
	public ?UriInterface $image = null;
	
	/**
	 * The url of the image of the flipped face, in case of flip card.
	 * 
	 * @var ?UriInterface
	 */
	public ?UriInterface $imageFlip = null;
	
	/**
	 * Whether this printing is a flip card.
	 * 
	 * @var ?boolean
	 */
	public ?bool $flip = null;
	
	/**
	 * Whether this printing is legal somewhere.
	 * 
	 * @var ?boolean
	 */
	public ?bool $legal = null;
	
	/**
	 * The legalities of this card.
	 * 
	 * @var ?ApiComMtgstocksCardLegality
	 */
	public ?ApiComMtgstocksCardLegality $legalities = null;
	
	/**
	 * Whether the related card is on the reserved list.
	 * 
	 * @var ?boolean
	 */
	public ?bool $reserved = null;
	
	/**
	 * The if of the set for this printing.
	 * 
	 * @var ?integer
	 */
	public ?int $setId = null;
	
	/**
	 * The name of the set for this printing.
	 * 
	 * @var ?string
	 */
	public ?string $setName = null;
	
	/**
	 * The type of the set for this printing.
	 * 
	 * @var ?string
	 */
	public ?string $setType = null;
	
	/**
	 * The set information this printing refers to.
	 * 
	 * @var ?ApiComMtgstocksSetResume
	 */
	public ?ApiComMtgstocksSetResume $cardSet = null;
	
	/**
	 * The card information this printing refers to.
	 * 
	 * @var ?ApiComMtgstocksCard
	 */
	public ?ApiComMtgstocksCard $card = null;
	
	/**
	 * Whether this printing is foil.
	 * 
	 * @var ?boolean
	 */
	public ?bool $foil = null;
	
	/**
	 * The icon class of this printing.
	 * 
	 * @var ?string
	 */
	public ?string $iconClass = null;
	
	/**
	 * The other printings this card is in.
	 * 
	 * @var array<integer, ApiComMtgstocksSetOwning>
	 */
	public array $sets = [];
	
}
