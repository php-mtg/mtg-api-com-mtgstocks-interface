<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgstocks-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgstocks;

use DateTimeInterface;

/**
 * ApiComMtgstocksAllTimeHigh class file.
 * 
 * This represents a higher point to the average price for all known
 * history.
 * 
 * @author Anastaszor
 */
class ApiComMtgstocksAllTimeHigh
{
	
	/**
	 * The average value of the price.
	 * 
	 * @var ?float
	 */
	public ?float $avg = null;
	
	/**
	 * The date when this price was valid.
	 * 
	 * @var ?DateTimeInterface
	 */
	public ?DateTimeInterface $date = null;
	
}
