<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgstocks-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgstocks;

/**
 * ApiComMtgstocksLatestPricePrez class file.
 *
 * This represents a price information for a given printing at some
 * moment in time.
 *
 * @author Anastaszor
 */
class ApiComMtgstocksLatestPricePrez
{
	
	/**
	 * The average price.
	 *
	 * @var ?float
	 */
	public ?float $avg = null;
	
	/**
	 * The foil price.
	 *
	 * @var ?float
	 */
	public ?float $foil = null;
	
	/**
	 * The market price.
	 *
	 * @var ?float
	 */
	public ?float $market = null;
	
	/**
	 * The price for the market foil card.
	 *
	 * @var ?float
	 */
	public ?float $marketFoil = null;
	
}
