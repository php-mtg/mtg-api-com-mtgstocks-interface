<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgstocks-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgstocks;

use DateTimeInterface;

/**
 * ApiComMtgstocksPrice class file.
 * 
 * This class is a simple implementation of the ApiComMtgstocksPriceHistory.
 * 
 * @author Anastaszor
 */
class ApiComMtgstocksPrice
{
	
	/**
	 * The date when the price was issued.
	 * 
	 * @var ?DateTimeInterface
	 */
	public ?DateTimeInterface $date = null;
	
	/**
	 * The value of the price.
	 * 
	 * @var ?float
	 */
	public ?float $value = null;
	
}
