<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgstocks-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgstocks;

/**
 * ApiComMtgstocksFormat class file.
 * 
 * This represents a format.
 * 
 * @author Anastaszor
 */
class ApiComMtgstocksFormat
{
	
	/**
	 * The id of the format.
	 * 
	 * @var ?integer
	 */
	public ?int $id = null;
	
	/**
	 * The name of the format.
	 * 
	 * @var ?string
	 */
	public ?string $name = null;
	
}
