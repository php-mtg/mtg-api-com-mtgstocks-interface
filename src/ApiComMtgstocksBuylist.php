<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgstocks-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgstocks;

/**
 * ApiComMtgstocksBuylist class file.
 * 
 * This represents the different prices of the card given its state.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiComMtgstocksBuylist
{
	
	/**
	 * The price near mint.
	 * 
	 * @var ?float
	 */
	public ?float $nm = null;
	
	/**
	 * The price low.
	 * 
	 * @var ?float
	 */
	public ?float $lp = null;
	
	/**
	 * The price near mint foil.
	 * 
	 * @var ?float
	 */
	public ?float $nmFoil = null;
	
	/**
	 * The price low foil.
	 * 
	 * @var ?float
	 */
	public ?float $lpFoil = null;
	
}
