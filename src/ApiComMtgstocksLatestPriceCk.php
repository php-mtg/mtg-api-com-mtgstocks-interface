<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgstocks-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgstocks;

use Psr\Http\Message\UriInterface;

/**
 * ApiComMtgstocksLatestPriceCk class file.
 * 
 * This represents the latest price seen on cardkingdom.com website.
 * 
 * @author Anastaszor
 */
class ApiComMtgstocksLatestPriceCk
{
	
	/**
	 * The price of target printing.
	 * 
	 * @var ?float
	 */
	public ?float $price = null;
	
	/**
	 * The foil price of target printing.
	 * 
	 * @var ?float
	 */
	public ?float $foil = null;
	
	/**
	 * The target url of the printing.
	 * 
	 * @var ?UriInterface
	 */
	public ?UriInterface $url = null;
	
	/**
	 * The target url for foil printing.
	 * 
	 * @var ?UriInterface
	 */
	public ?UriInterface $urlFoil = null;
	
}
