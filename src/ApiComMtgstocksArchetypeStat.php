<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgstocks-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgstocks;

/**
 * ApiComMtgstocksArchetypeStat class file.
 * 
 * This is a bridge between the card and the archetypes that uses it.
 * 
 * @author Anastaszor
 */
class ApiComMtgstocksArchetypeStat
{
	
	/**
	 * The id of the archetype.
	 * 
	 * @var ?integer
	 */
	public ?int $id = null;
	
	/**
	 * The quantity of recent decks in this archetype the caller printing is in.
	 * 
	 * @var ?integer
	 */
	public ?int $counter = null;
	
	/**
	 * The name of the archetype.
	 * 
	 * @var ?string
	 */
	public ?string $name = null;
	
	/**
	 * The id of the format the archetype is in.
	 * 
	 * @var ?integer
	 */
	public ?int $formatId = null;
	
	/**
	 * The name of the format the archetype is in.
	 * 
	 * @var ?string
	 */
	public ?string $formatName = null;
	
}
