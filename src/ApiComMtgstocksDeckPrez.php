<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgstocks-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgstocks;

/**
 * ApiComMtgstocksDeckPrez class file.
 * 
 * This represents the first attributes of the deck presentation from
 * the archetype page.
 * 
 * @author Anastaszor
 */
class ApiComMtgstocksDeckPrez
{
	
	/**
	 * The id of the deck.
	 * 
	 * @var ?integer
	 */
	public ?int $id = null;
	
	/**
	 * The type of object (i.e. "decks").
	 * 
	 * @var ?string
	 */
	public ?string $type = null;
	
	/**
	 * The attributes of the deck.
	 * 
	 * @var ?ApiComMtgstocksDeckAttributes
	 */
	public ?ApiComMtgstocksDeckAttributes $attributes = null;
	
}
