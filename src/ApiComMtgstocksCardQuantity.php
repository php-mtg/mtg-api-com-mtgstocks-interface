<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgstocks-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgstocks;

/**
 * ApiComMtgstocksCardQuantity class file.
 * 
 * This represents a row of a deck with the number of same cards.
 * 
 * @author Anastaszor
 */
class ApiComMtgstocksCardQuantity
{
	
	/**
	 * The id of the card.
	 * 
	 * @var ?integer
	 */
	public ?int $id = null;
	
	/**
	 * The card that this quantity is linked to.
	 * 
	 * @var ?ApiComMtgstocksDeckCard
	 */
	public ?ApiComMtgstocksDeckCard $card = null;
	
	/**
	 * The quantity of cards.
	 * 
	 * @var ?integer
	 */
	public ?int $quantity = null;
	
}
